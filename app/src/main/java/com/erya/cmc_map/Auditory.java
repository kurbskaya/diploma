package com.erya.cmc_map;

public class Auditory {
    private int id;
    private int neighbour;
    private int weight;
    private boolean status;

    Auditory(int id, int neighbour, int weight, boolean status){
        this.id = id ;
        this.neighbour = neighbour ;
        this.weight = weight;
        this.status = status;
    }

    public int getID() {
        return id;
    }

    public void setID(int id) {
        this.id = id;
    }

    public int getNeighbour() {
        return neighbour;
    }

    public void setNeighbour(int neighbour) {
        this.neighbour = neighbour;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public  String toString(){
        return id + " " + String.valueOf(neighbour) + " " + String.valueOf(weight) + " " + String.valueOf(status);
    }
}
