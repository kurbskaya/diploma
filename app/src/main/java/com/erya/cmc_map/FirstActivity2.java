package com.erya.cmc_map;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;

import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class FirstActivity2 extends AppCompatActivity {

    private ArrayAdapter<Auditory> adapter;
    private EditText idText, neighbourText, weightText, statusText;
    private List<Auditory> auds;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_first2);

        idText = (EditText) findViewById (R.id.idText);
        neighbourText = (EditText) findViewById (R.id.neighbourText);
        weightText = (EditText) findViewById (R.id.weightText);
        statusText = (EditText) findViewById (R.id.statusText);

        auds = new ArrayList<>();
        listView = (ListView) findViewById (R.id.list);
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, auds);
        listView.setAdapter(adapter);
    }

    public void addAud(View view){
        int id = Integer.parseInt(idText.getText().toString());
        int neighbour = Integer.parseInt(neighbourText.getText().toString());
        int weight = Integer.parseInt(weightText.getText().toString());
        boolean status = Boolean.parseBoolean(statusText.getText().toString());
        Auditory aud = new Auditory(id, neighbour, weight, status);
        auds.add(aud);
        adapter.notifyDataSetChanged();
    }

    public void save(View view) {

        boolean result = JSONHelper.exportToJSON(this, auds);
        if(result){
            Toast.makeText(this, "Данные сохранены", Toast.LENGTH_LONG).show();
        }
        else{
            Toast.makeText(this, "Не удалось сохранить данные", Toast.LENGTH_LONG).show();
        }
    }
    public void open(View view){
        auds = JSONHelper.importFromJSON(this);
        if(auds!=null){
            adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, auds);
            listView.setAdapter(adapter);
            Toast.makeText(this, "Данные восстановлены", Toast.LENGTH_LONG).show();
        }
        else{
            Toast.makeText(this, "Не удалось открыть данные", Toast.LENGTH_LONG).show();
        }
    }
}