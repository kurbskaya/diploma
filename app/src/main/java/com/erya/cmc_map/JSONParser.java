package com.erya.cmc_map;

import org.json.JSONException;
import org.json.JSONObject;

public class JSONParser {

    public static Auditory getAud(String response, int key) throws JSONException {
        JSONObject userJson = new JSONObject(response);
        int id = userJson.getInt("id");
        int neighbour = userJson.getInt("neighbour");
        int weight = userJson.getInt("weight");
        boolean status = userJson.getBoolean("status");


        return new Auditory(id, neighbour, weight, status);
    }
}
