package com.erya.cmc_map;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class FirstActivity1 extends AppCompatActivity {
    private String j_Line;
    private EditText idText, neighbourText, weightText, statusText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_first1);

        idText = (EditText) findViewById (R.id.idText);

        j_Line = "";
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(
                    new InputStreamReader(getAssets().open("data.json")));

            String mLine;
            while ((mLine = reader.readLine()) != null) {
                j_Line = j_Line.concat(mLine);
                System.out.println(j_Line); //j_Line -строка
            }
        } catch (IOException e) {

        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {

                }
            }
        }

    }

    public void searchData(View view) throws JSONException {

        int id = Integer.parseInt(idText.getText().toString());

        TextView textView = (TextView) findViewById(R.id.textView);
        Auditory aud = JSONParser.getAud (j_Line, id);
        String str_id = Integer.toString(aud.getID());
        String str_neighbour = Integer.toString(aud.getNeighbour());
        String str_status = Boolean.toString(aud.getStatus());
        textView.setText("ID: " + str_id + "\n" + "neighbour: " + str_neighbour + "\n" + "status: " + str_status);
    }
}