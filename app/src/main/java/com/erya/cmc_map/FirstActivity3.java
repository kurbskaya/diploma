package com.erya.cmc_map;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import java.io.IOException;

public class FirstActivity3 extends AppCompatActivity {
    private EditText ip_name;
    private String ip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first3);

        ip_name = (EditText) findViewById (R.id.ip_name);

    }

    @SuppressLint("StaticFieldLeak")
    public void Enter(View view){
        ip = ip_name.getText().toString();
        try {
            final HTTPClient IP_obj = new HTTPClient(ip);
            new AsyncTask<Void, String, String>() {
                @Override
                protected String doInBackground(Void... voids) {
                    String s = "";
                    try {
                        s = IP_obj.doGet();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return s;
                }

                @Override
                protected void onPostExecute(final String result) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Log.d("RES", result);
                        }
                    });
                }
            }.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}